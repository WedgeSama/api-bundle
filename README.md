DrosalysApiBundle
=================

A Symfony's bundle to ease API creation. 

## Install

```
composer require drosalys/api-bundle
```

Flex not yet available.

- Enable the bundle in your `config/bundle.php`:
```php
<?php

return [
    //...
    Drosalys\Bundle\ApiBundle\DrosalysApiBundle::class => ['all' => true],
    //...
];
```

- Create a new `config\packages\drosalys_api.yaml` file, you will config the bundle here.
```yaml
drosalys_api: ~
```

- That all for default configuration.

## Examples

### GET action route example

```php
<?php
namespace App\Controller\Api;

use App\Entity\YourEntity;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class GetAction
{
    /**
     * Description of your action. Displayed in OpenAPI doc UI.
     */
    #[Get('/my-path/{id}')]
    #[Serializable(groups: 'YourGroup')]
    public function __invoke(YourEntity $yourEntity): ?YourEntity
    {
        return $yourEntity;
    }
}

```

### GET Collection action route example

#### Controller
```php
<?php
namespace App\Controller\Api;

use App\Entity\YourEntity;
use App\Filter\YourEntityFilter;
use App\Repository\YourEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Drosalys\Bundle\ApiBundle\Filter\Attributes\Filterable;
use Drosalys\Bundle\ApiBundle\Pagination\Attributes\Paginable;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class CollectionAction
{
    public function __construct(
        private YourEntityRepository $repository,
    ) {}

    /**
     * Description of your action. Displayed in OpenAPI doc UI.
     */
    #[Get('/my-path')]
    #[Serializable(groups: 'YourGroup')]
    #[Paginable(YourEntity::class)] // The type of items inside collection.
    #[Filterable(YourEntityFilter::class)]
    public function __invoke(): QueryBuilder
    {
        return $this->repository->getAll();
    }
}

```

#### FormFilter (YourEntityFilter)
```php
<?php
namespace App\Filter;

use Drosalys\Bundle\ApiBundle\Filter\ApiFilter;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class YourEntityFilter extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS,
            ])
            ->add('username', TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS,
            ])
        ;
    }

    public function getParent()
    {
        return ApiFilter::class;
    }
}

```

#### Repository (YourEntityRepository)

```php
<?php

namespace App\Repository\Account;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;

class YourEntityRepository extends ServiceEntityRepository
{
    /*...*/
    public function getAll(): QueryBuilder
    {
        return $this->createQueryBuilder('u')
            ->where(/* build query */)
        ;
    }
    /*...*/
}

```

### Others API actions attributes.

```php
<?php

use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Post;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Put;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Delete;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Patch;

```

## TODO

- Better documentation.

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
