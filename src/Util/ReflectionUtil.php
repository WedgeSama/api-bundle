<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Util;

/**
 * Class ReflectionUtil
 *
 * @author Benjamin Georgeault
 */
abstract class ReflectionUtil
{
    public static function hasAttribute(\ReflectionMethod|\ReflectionProperty|\ReflectionClass $ref, string $type): bool
    {
        return !empty($ref->getAttributes($type, \ReflectionAttribute::IS_INSTANCEOF));
    }

    public static function getAttributes(\ReflectionMethod|\ReflectionProperty|\ReflectionClass $ref, string $type): array
    {
        return array_map(function (\ReflectionAttribute $attribute) {
            return $attribute->newInstance();
        }, $ref->getAttributes($type, \ReflectionAttribute::IS_INSTANCEOF));
    }

    public static function getOneAttribute(\ReflectionMethod|\ReflectionProperty|\ReflectionClass $ref, string $type): ?object
    {
        $attributes = static::getAttributes($ref, $type);

        if (empty($attributes)) {
            return null;
        }

        if (count($attributes) > 1) {
            throw new \LogicException(sprintf(
                'More than one "%s" attributes found on "%s::%s".',
                $type,
                $ref->class,
                $ref->name,
            ));
        }

        return $attributes[0];
    }
}
