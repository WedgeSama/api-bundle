<?php

/*
 * This file is part of the drosalys-api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Event\Attributes;

/**
 * Class AbstractPersist
 *
 * @author Benjamin Georgeault
 *
 * TODO handle service callable too.
 */
abstract class AbstractAttribute
{
    private string $method;

    public function __construct(?string $method = null)
    {
        if (null === $method) {
            $ref = new \ReflectionObject($this);
            $method = lcfirst($ref->getShortName());
        }

        $this->method = $method;
    }

    public function getMethod(): string
    {
        return $this->method;
    }
}
