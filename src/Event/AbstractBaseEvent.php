<?php

/*
 * This file is part of the drosalys-api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Event;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class AbstractBaseEvent
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractBaseEvent extends Event
{
    public function __construct(
        private Action $action,
        private Request $request,
        private mixed $data,
    ) {
    }

    public function getAction(): Action
    {
        return $this->action;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getData(): mixed
    {
        return $this->data;
    }
}
