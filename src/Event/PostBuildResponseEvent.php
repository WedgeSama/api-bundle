<?php

/*
 * This file is part of the drosalys-api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Event;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PostResponseBuildEvent
 *
 * @author Benjamin Georgeault
 */
final class PostBuildResponseEvent extends AbstractViewEvent
{
    public function __construct(
        Action $action,
        Request $request,
        mixed $data,
        private Response $response,
    ) {
        parent::__construct($action, $request, $data);
    }

    public function getResponse(): Response
    {
        return $this->response;
    }
}
