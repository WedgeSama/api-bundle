<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Routing\Attributes;

/**
 * Class Options
 *
 * @author Benjamin Georgeault
 *
 * @Annotation
 * @NamedArgumentConstructor
 * @Target({"METHOD"})
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class Options extends AbstractRoute
{
    public function getMethod(): string
    {
        return 'OPTIONS';
    }
}

