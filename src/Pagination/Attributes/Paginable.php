<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Pagination\Attributes;

/**
 * Class Paginable
 *
 * @author Benjamin Georgeault
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class Paginable
{
    public function __construct(
        private string $innerType,
        private ?int $page = null,
        private ?int $limit = null,
        private ?int $maxLimit = null,
        private ?string $pageName = null,
        private ?string $limitName = null,
        private array $options = [],
    ) {
    }

    public function getInnerType(): string
    {
        return $this->innerType;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function getMaxLimit(): ?int
    {
        return $this->maxLimit;
    }

    public function getPageName(): ?string
    {
        return $this->pageName;
    }

    public function getLimitName(): ?string
    {
        return $this->limitName;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
