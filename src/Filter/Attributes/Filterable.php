<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Filter\Attributes;

use Symfony\Component\Form\FormTypeInterface;

/**
 * Class Filterable
 *
 * @author Benjamin Georgeault
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class Filterable
{
    public function __construct(
        private string $filterType,
        private array $options = [],
        private ?string $key = null,
    ) {
        try {
            $ref = new \ReflectionClass($this->filterType);
        } catch (\ReflectionException $e) {
            throw new \LogicException('Filtrable filterType must be a class.');
        }

        if (!$ref->implementsInterface(FormTypeInterface::class)) {
            throw new \LogicException(sprintf(
                'Filtrable filterType must implement "%s" interface.',
                FormTypeInterface::class,
            ));
        }
    }

    public function getFilterType(): string
    {
        return $this->filterType;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }
}
