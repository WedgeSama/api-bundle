<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Filter;

use Doctrine\ORM\QueryBuilder;
use Drosalys\Bundle\ApiBundle\Action\Info\FilterInfo;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiFilterManager
 *
 * @author Benjamin Georgeault
 */
final class ApiFilterManager
{
    public function __construct(
        private FilterBuilderUpdaterInterface $builderUpdater,
        private FormFactoryInterface $formFactory,
    ) {
    }

    public function createFormFilter(FilterInfo $info): FormInterface
    {
        return $this->createRawFormFilter($info->getFilterType(), $info->getOptions(), $info->getKey());
    }

    public function applyFilter(QueryBuilder $qb, Request $request, FilterInfo $info): QueryBuilder
    {
        return $this->applyRawFilter($qb, $request, $info->getFilterType(), $info->getOptions(), $info->getKey());
    }

    public function applyRawFilter(QueryBuilder $qb, Request $request, string $formType, array $options = [], string $key = 'f'): QueryBuilder
    {
        $form = $this->createRawFormFilter($formType, $options, $key);
        $form->handleRequest($request);
        $this->builderUpdater->addFilterConditions($form, $qb);

        return $qb;
    }

    private function createRawFormFilter(string $formType, array $options = [], string $key = 'f'): FormInterface
    {
        return $this->formFactory->createNamed($key, $formType, null, $options);
    }
}
