<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Persister\PersisterHandler;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface PersisterHandlerInterface
 *
 * @author Benjamin Georgeault
 */
interface PersisterHandlerInterface
{
    public function support(Action $action, Request $request, mixed $data): bool;

    public function persist(Action $action, Request $request, mixed $data): void;
}
