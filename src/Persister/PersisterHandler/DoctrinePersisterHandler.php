<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Persister\PersisterHandler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Mapping\MappingException;
use Drosalys\Bundle\ApiBundle\Action\Action;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DoctrinePersisterHandler
 *
 * @author Benjamin Georgeault
 */
class DoctrinePersisterHandler implements PersisterHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
    ) {
    }

    public function support(Action $action, Request $request, mixed $data): bool
    {
        try {
            return is_object($data)
                && (
                    $request->isMethod('POST')
                    || $request->isMethod('PUT')
                    || $request->isMethod('DELETE')
                )
                && $this->em->getMetadataFactory()->getMetadataFor(get_class($data))
            ;
        } catch (MappingException $e) {
            return false;
        }
    }

    public function persist(Action $action, Request $request, mixed $data): void
    {
        if ($request->isMethod('DELETE')) {
            $this->em->remove($data);
        } else {
            $this->em->persist($data);
        }

        $this->em->flush();
    }
}
