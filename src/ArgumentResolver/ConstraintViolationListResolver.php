<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ArgumentResolver;

use Drosalys\Bundle\ApiBundle\EventSubscriber\ActionControllerSubscriber;
use Drosalys\Bundle\ApiBundle\EventSubscriber\DeserializeActionControllerSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class ConstraintViolationListResolver
 *
 * @author Benjamin Georgeault
 */
class ConstraintViolationListResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $request->attributes->has(ActionControllerSubscriber::REQUEST_KEY)
            && $request->attributes->has(DeserializeActionControllerSubscriber::REQUEST_ERROR_KEY)
            && (
                $argument->getType() === ConstraintViolationListInterface::class
                || $argument->getType() === ConstraintViolationList::class
            )
        ;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        yield $request->attributes->get(DeserializeActionControllerSubscriber::REQUEST_ERROR_KEY);
    }
}
