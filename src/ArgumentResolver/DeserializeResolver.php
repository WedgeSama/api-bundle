<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ArgumentResolver;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Drosalys\Bundle\ApiBundle\Action\Info\DeserializeInfo;
use Drosalys\Bundle\ApiBundle\EventSubscriber\ActionControllerSubscriber;
use Drosalys\Bundle\ApiBundle\EventSubscriber\DeserializeActionControllerSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * Class DeserializeResolver
 *
 * @author Benjamin Georgeault
 */
class DeserializeResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        if (
            !$request->attributes->has(DeserializeActionControllerSubscriber::REQUEST_KEY)
            || (null === $deserializeInfo = $this->getDeserializeInfo($request))
        ) {
            return false;
        }

        $type = $deserializeInfo->getType();
        return $argument->getType() === $type->getClassName();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (null === $this->getDeserializeInfo($request)) {
            return;
        }

        yield $request->attributes->get(DeserializeActionControllerSubscriber::REQUEST_KEY);
    }

    private function getDeserializeInfo(Request $request): ?DeserializeInfo
    {
        if (!$request->attributes->has(ActionControllerSubscriber::REQUEST_KEY)) {
            return null;
        }

        $action = $request->attributes->get(ActionControllerSubscriber::REQUEST_KEY);
        if (!$action instanceof Action) {
            return null;
        }

        if ($deserializeInfo = $action->getDeserializeInfo()) {
            return $deserializeInfo;
        }

        return null;
    }
}
