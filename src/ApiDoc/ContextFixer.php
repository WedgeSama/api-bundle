<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc;

use OpenApi\Annotations\AbstractAnnotation;

/**
 * Class ContextFixer
 *
 * @author Benjamin Georgeault
 */
abstract class ContextFixer
{
    final public static function fixContext(AbstractAnnotation $child, AbstractAnnotation $parent): AbstractAnnotation
    {
        $context = $child->_context;
        $ref = new \ReflectionObject($context);
        $refContext = $ref->getProperty('_parent');

        $refContext->setAccessible(true);
        $refContext->setValue($context, $parent->_context);
        $refContext->setAccessible(false);

        $context->nested = $parent;

        return $child;
    }
}
