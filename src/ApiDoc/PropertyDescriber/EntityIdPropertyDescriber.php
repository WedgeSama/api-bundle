<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\PropertyDescriber;

use Doctrine\ORM\EntityManagerInterface;
use Drosalys\Bundle\ApiBundle\ApiDoc\ContextFixer;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\IdGroups;
use Drosalys\Bundle\ApiBundle\Util\ReflectionUtil;
use Nelmio\ApiDocBundle\Model\Model;
use Nelmio\ApiDocBundle\Model\ModelRegistry;
use Nelmio\ApiDocBundle\PropertyDescriber\ObjectPropertyDescriber;
use OpenApi\Annotations as OA;
use OpenApi\Context;
use OpenApi\Generator;

/**
 * Class EntityIdPropertyDescriber
 *
 * @author Benjamin Georgeault
 */
class EntityIdPropertyDescriber extends ObjectPropertyDescriber
{
    private EntityManagerInterface $em;
    private ModelRegistry $modelRegistry;
    private \ReflectionProperty $ref;

    public function setEm(EntityManagerInterface $em): void
    {
        $this->em = $em;
    }

    public function setModelRegistry(ModelRegistry $modelRegistry)
    {
        $this->modelRegistry = $modelRegistry;
        $this->ref = (new \ReflectionObject($modelRegistry))->getProperty('registeredModelNames');
        parent::setModelRegistry($modelRegistry);
    }

    public function describe(array $types, OA\Schema $property, array $groups = null)
    {
        if (
            null === $groups
            || (!$property instanceof OA\Property)
            || (Generator::UNDEFINED === $property->property)
            || (null === $class = $this->retrieveParentClass($property))
        ) {
            parent::describe($types, $property, $groups);
            return;
        }

        $classRef = new \ReflectionClass($class);
        $propRef = $classRef->getProperty($property->property);

        if (ReflectionUtil::hasAttribute($propRef, IdGroups::class)) {
            /** @var IdGroups $attribute */
            foreach (ReflectionUtil::getAttributes($propRef, IdGroups::class) as $attribute) {
                if (!empty(array_intersect($attribute->getGroups(), $groups))) {
                    $this->updateProperty($property, $class);
                    break;
                }
            }
        } else {
            parent::describe($types, $property, $groups);
        }
    }

    private function retrieveParentClass(OA\Schema $property): ?string
    {
        if (!$property->_context instanceof Context) {
            return null;
        }

        $nested = $property->_context->nested;
        if (!$nested instanceof OA\Schema) {
            return null;
        }

        if (Generator::UNDEFINED === $nested->schema) {
            return null;
        }

        // TODO find another way to do this.
        $this->ref->setAccessible(true);
        /** @var Model[] $models */
        $models = $this->ref->getValue($this->modelRegistry);
        $this->ref->setAccessible(false);

        if (!array_key_exists($nested->schema, $models)) {
            return null;
        }

        return $models[$nested->schema]->getType()->getClassName();
    }

    private function updateProperty(OA\Schema $property, string $class): void
    {
        $doctrineMetadata = $this->em->getClassMetadata($class);

        $targetClass = $doctrineMetadata->getAssociationTargetClass($property->property);
        $targetDoctrineMetadata = $this->em->getClassMetadata($targetClass);


        if ($targetDoctrineMetadata->isIdentifierComposite) {
            // TODO Not yet implemented.
            return;
        }

        $type = $targetDoctrineMetadata->getTypeOfField($targetDoctrineMetadata->getSingleIdentifierFieldName());

        if ($doctrineMetadata->isSingleValuedAssociation($property->property)) {
            $property->type = $type;
            $property->description = 'Entity\'s identifier.';
        } else {
            $property->type = 'array';
            $property->items = ContextFixer::fixContext(new OA\Items([
                'type' => $type,
            ]), $property);
        }
    }
}
