<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber;

use Drosalys\Bundle\ApiBundle\Action\Action;
use OpenApi\Generator;
use Symfony\Component\Routing\Route;

/**
 * Class TagDescriber
 *
 * @author Benjamin Georgeault
 */
class TagDescriber extends AbstractDescriber
{
    private array $namespaces;

    public function __construct(array $namespaces = [])
    {
        $namespaces[] = 'App\\Controller';

        $this->namespaces = array_map(function (string $namespace) {
            return preg_quote(rtrim($namespace, '\\'));
        }, $namespaces);
    }

    protected function doDescribe(Route $route, Action $action): void
    {
        if (
            (null === $pathItem = $this->findPathItem($action->getPath(), false))
            || (null === $operation = $this->findOperation($pathItem, $action->getMethod(), false))
            || Generator::UNDEFINED !== $operation->tags
        ) {
            return;
        }

        $ref = $this->getReflectionMethod();
        $classRef = $ref->getDeclaringClass();
        $fullName = preg_replace('/(Controller|Action)$/', '', $classRef->getName());

        foreach ($this->namespaces as $namespace) {
            if (!preg_match('/^'.$namespace.'\\\\([^\\\\]+)\\\\/', $fullName, $matches)) {
                continue;
            }

            $operation->tags = [
                $matches[1],
            ];

            break;
        }
    }
}
