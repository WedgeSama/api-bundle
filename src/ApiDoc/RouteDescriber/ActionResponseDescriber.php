<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Drosalys\Bundle\ApiBundle\Action\Info\SerializeInfo;
use Drosalys\Bundle\ApiBundle\Action\Type\Type;
use OpenApi\Annotations\Operation;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;
use OpenApi\Generator;
use Symfony\Component\Routing\Route;

/**
 * Class ActionResponseDescriber
 *
 * @author Benjamin Georgeault
 */
class ActionResponseDescriber extends AbstractDescriber
{
    private static array $defaultItemOkDescriptions = [
        'GET' => 'Get one resource.',
        'POST' => 'Create one resource.',
        'PUT' => 'Edit one resource.',
        'DELETE' => 'Delete one resource.',
    ];

    protected function doDescribe(Route $route, Action $action): void
    {
        $pathItem = $this->findPathItem($action->getPath());
        $operation = $this->findOperation($pathItem, $action->getMethod());

        $this->describeResponses($operation);
    }

    private function describeResponses(Operation $operation): void
    {
        $action = $this->getAction();
        if (empty($info = $action->getSerializeInfo()) || null === $type = $info->getType()) {
            return;
        }

        if ($type->isVoid()) {
            $noContentResponse = $this->findResponse($operation, 204);

            if (Generator::UNDEFINED === $noContentResponse->description || empty($noContentResponse->description)) {
                $noContentResponse->description = 'No content';
            }
        } elseif ($type->isItemable()) {
            $okResponse = $this->findResponse($operation, $action->isMethod('POST') ? 201 : 200);

            if (
                (Generator::UNDEFINED === $okResponse->description || empty($okResponse->description))
                && array_key_exists($action->getMethod(), self::$defaultItemOkDescriptions)
            ) {
                $okResponse->description = self::$defaultItemOkDescriptions[$action->getMethod()];
            }

            $this->describeContent($okResponse, $info, $type);
        }

        if ($type->isNullable() || $type->isVoid()) {
            $notFoundResponse = $this->findResponse($operation, 404);

            if (Generator::UNDEFINED === $notFoundResponse->description || empty($noContentResponse->description)) {
                $notFoundResponse->description = 'Not Found';
            }
        }
    }

    private function describeContent(Response $response, SerializeInfo $info, Type $type): void
    {
        $mediaType = $this->findMediaType($response, 'application/json');
        if (Generator::UNDEFINED !== $mediaType->schema) {
            return;
        }

        if ($type->isUnion()) {
            $schema = new Schema([
                'schema' => null,
            ]);
            $schema->oneOf = [];
            foreach ($type->getTypes() as $subType) {
                $schema->oneOf[] = $this->fixContext($this->createSchema($info, $subType), $mediaType);
            }

            if (!empty($schema->oneOf)) {
                $mediaType->schema = $this->fixContext($schema, $mediaType);
            }
        } else {
            $mediaType->schema = $this->fixContext($this->createSchema($info, $type), $response);
        }
    }
}
