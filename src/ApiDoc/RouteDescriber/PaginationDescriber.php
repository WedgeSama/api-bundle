<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Drosalys\Bundle\ApiBundle\Action\Info\PaginationInfo;
use Drosalys\Bundle\ApiBundle\Filter\ApiFilterManager;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\BooleanFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\CheckboxFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateTimeFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\NumberFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\Operation;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;
use OpenApi\Generator;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\Route;
use function Symfony\Component\String\u;

/**
 * Class PaginationDescriber
 *
 * @author Benjamin Georgeault
 */
class PaginationDescriber extends AbstractDescriber
{
    public function __construct(
        private ApiFilterManager $filterManager,
    ) {
    }

    protected function doDescribe(Route $route, Action $action): void
    {
        $pathItem = $this->findPathItem($action->getPath());
        $operation = $this->findOperation($pathItem, $action->getMethod());

        if (null !== $paginationInfo = $this->getAction()->getPaginationInfo()) {
            $this->describeParameters($operation, $paginationInfo);
            $this->describeFilters($operation);
            $this->describeResponse($operation);
        }
    }

    private function describeParameters(Operation $operation, PaginationInfo $paginationInfo): void
    {
        // Page
        $param = $this->findParameter($operation, $paginationInfo->getPageName());
        if (Generator::UNDEFINED === $param->schema) {
            $this->fixContext($param->schema = new Schema([
                'schema' => null,
                'type' => 'number',
                'minimum' => 1,
            ]), $param);
        }

        // Limit
        $param = $this->findParameter($operation, $paginationInfo->getLimitName());
        if (Generator::UNDEFINED === $param->schema) {
            $this->fixContext($param->schema = new Schema([
                'schema' => null,
                'type' => 'number',
                'minimum' => 1,
                'maximum' => $paginationInfo->getMaxLimit(),
            ]), $param);
        }
    }

    private function describeResponse(Operation $operation): void
    {
        $action = $this->getAction();
        if (empty($info = $action->getSerializeInfo()) || null === $type = $info->getType()) {
            return;
        }

        $okResponse = $this->findResponse($operation, $action->isMethod('POST') ? 201 : 200);
        if (Generator::UNDEFINED === $okResponse->description || empty($okResponse->description)) {
            $okResponse->description = 'Get collection of object.';
        }

        $mediaType = $this->findMediaType($okResponse, 'application/json');
        if (Generator::UNDEFINED !== $mediaType->schema) {
            return;
        }

        $schema = new Schema([
            'schema' => null,
            'type' => 'object',
        ]);

        $schema->properties = [];

        foreach (['page', 'items_per_page', 'total', 'total_page'] as $prop) {
            $schema->properties[] = $this->fixContext(new Property([
                'schema' => null,
                'property' => $prop,
                'type' => 'integer',
            ]), $schema);
        }

        /** @var Property $itemsProp */
        $itemsProp = $schema->properties[] = $this->fixContext(new Property([
            'schema' => null,
            'property' => 'items',
            'type' => 'array',
        ]), $schema);

        $itemsProp->items = $this->fixContext(new Items([]), $itemsProp);
        $itemsProp->items->ref = $this->registerModel($action->getSerializeInfo(), $type);

        if (Generator::UNDEFINED === $schema->title) {
            $schema->title = $this->getNameFor($action->getSerializeInfo(), $type).'Collection';
        }

        $mediaType->schema = $this->fixContext($schema, $mediaType);
    }

    private function describeFilters(Operation $operation): void
    {
        if (null === $filterInfo = $this->getAction()->getFilterInfo()) {
            return;
        }

        $form = $this->filterManager->createFormFilter($filterInfo);
        $this->extractParameters($operation, $form);
    }

    private function extractParameters(Operation $operation, FormInterface $form, string $prefix = ''): array
    {
        $params = [];
        $config = $form->getConfig();
        $type = $config->getType()->getInnerType();
        $view = $form->createView();

        if (empty($prefix)) {
            $name = $form->getName();
        } else {
            $name = sprintf('%s[%s]', $prefix, $form->getName());
        }

        if ($type instanceof TextFilterType) {
            $param = $this->findParameter($operation, $name);
            if (Generator::UNDEFINED === $param->schema) {
                $this->fixContext($param->schema = new Schema([
                    'schema' => null,
                    'type' => 'string',
                ]), $param);
            }
        } elseif ($type instanceof ChoiceFilterType || $type instanceof BooleanFilterType) {
            $param = $this->findParameter($operation, $name);
            if (Generator::UNDEFINED === $param->schema) {
                $this->fixContext($param->schema = new Schema([
                    'schema' => null,
                    'type' => 'string',
                ]), $param);

                if (!empty($choices = $view->vars['choices'])) {
                    $param->schema->enum = [];

                    /** @var ChoiceView $choice */
                    foreach ($choices as $choice) {
                        $param->schema->enum[] = $choice->value;
                    }
                }
            }
        } elseif ($type instanceof CheckboxFilterType) {
            $param = $this->findParameter($operation, $name);
            if (Generator::UNDEFINED === $param->schema) {
                $this->fixContext($param->schema = new Schema([
                    'schema' => null,
                    'type' => 'number',
                ]), $param);

                $param->schema->enum = [
                    1,
                ];
            }
        } elseif (($type instanceof DateFilterType || $type instanceof DateTimeFilterType) && !$config->getCompound()) {
            $param = $this->findParameter($operation, $name);
            if (Generator::UNDEFINED === $param->schema) {
                if ($type instanceof DateTimeFilterType) {
                    $format = 'date-time';
                    $description = '2021-01-01T01:12:00';
                } else {
                    $format = 'date';
                    $description = '2021-01-01';
                }

                $this->fixContext($param->schema = new Schema([
                    'schema' => null,
                    'type' => 'string',
                    'format' => $format,
                ]), $param);

                $param->description = 'eg. ' . $description;
            }
        } elseif ($type instanceof EntityFilterType) {
            if ($multiple = $config->getOption('multiple', false)) {
                $param = $this->findParameter($operation, $name.'[]');
            } else {
                $param = $this->findParameter($operation, $name);
            }

            if (Generator::UNDEFINED === $param->schema) {
                $entityName = u($config->getOption('class', ''))->split('\\');
                $baseEntityName = array_pop($entityName) ?? 'entity';

                if ($multiple) {
                    $this->fixContext($param->schema = new Schema([
                        'schema' => null,
                        'type' => 'array',
                    ]), $param);

                    $param->schema->items = $this->fixContext(new Items([
                        'type' => 'string',
                    ]), $param->schema);

                    $param->description = sprintf(
                        'An array of %s\'s id.',
                        $baseEntityName,
                    );
                } else {
                    $this->fixContext($param->schema = new Schema([
                        'schema' => null,
                        'type' => 'string',
                    ]), $param);

                    $param->description = sprintf(
                        'A %s\'s id.',
                        $baseEntityName,
                    );
                }
            }
        } elseif ($type instanceof NumberFilterType) {
            $param = $this->findParameter($operation, $name);
            if (Generator::UNDEFINED === $param->schema) {
                $this->fixContext($param->schema = new Schema([
                    'schema' => null,
                    'type' => 'number',
                ]), $param);
            }
        } elseif ($config->getCompound()) {
            foreach ($form as $child) {
                $params = array_merge($params, $this->extractParameters($operation, $child, $name));
            }
        }

        return $params;
    }
}
