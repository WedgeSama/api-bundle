<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber;

use Drosalys\Bundle\ApiBundle\Action\Action;
use OpenApi\Generator;
use Symfony\Component\Routing\Route;

/**
 * Class SecurityDescriber
 *
 * @author Benjamin Georgeault
 */
class SecurityDescriber extends AbstractDescriber
{
    protected function doDescribe(Route $route, Action $action): void
    {
        $api = $this->getApi();

        // Ignore this if no security scheme registered or if no operation found or already security info.
        if (
            (Generator::UNDEFINED === $api->components)
            || (Generator::UNDEFINED === $api->components->securitySchemes)
            || empty($api->components->securitySchemes)
            || (null === $pathItem = $this->findPathItem($action->getPath(), false))
            || (null === $operation = $this->findOperation($pathItem, $action->getMethod(), false))
            || (Generator::UNDEFINED !== $operation->security)
            || !$action->hasSecurity()
        ) {
            return;
        }

        // If more then one security schemed, cannot determined which one to use.
        if (1 < count($api->components->securitySchemes)) {
            throw new \LogicException(sprintf(
                'There are more then one security schemes registered. Cannot guess which one to use on "%s". You need to specify it with @Security annotation from Nelmio bundle.',
                $route->getPath()
            ));
        }

        if (Generator::UNDEFINED === $operation->security) {
            $operation->security = [];
        }

        $key = $api->components->securitySchemes[0]->securityScheme;

        $operation->security[] = [
            $key => [],
        ];
    }
}
