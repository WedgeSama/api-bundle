<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber;

use Drosalys\Bundle\ApiBundle\Action\Action;
use OpenApi\Generator;
use Symfony\Component\Routing\Route;

/**
 * Class ActionRequestBodyDescriber
 *
 * @author Benjamin Georgeault
 */
class ActionRequestBodyDescriber extends AbstractDescriber
{
    protected function doDescribe(Route $route, Action $action): void
    {
        if (
            !($action->isMethod('POST') || $action->isMethod('PUT'))
            || null === $info = $action->getDeserializeInfo()
        ) {
            return;
        }

        $pathItem = $this->findPathItem($action->getPath());
        $operation = $this->findOperation($pathItem, $action->getMethod());
        $requestBody = $this->findRequestBody($operation);

        $mediaType = $this->findMediaType($requestBody, 'application/json');
        if (Generator::UNDEFINED !== $mediaType->schema) {
            return;
        }

        $type = $info->getType();
        if ($type->isUnion()) {
            throw new \LogicException('Error, cannot union on type for deserialize.');
        } else {
            $mediaType->schema = $this->fixContext($this->createSchema($info, $type), $requestBody);
        }
    }
}
