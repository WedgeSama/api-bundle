<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Drosalys\Bundle\ApiBundle\Action\ActionRetriever;
use Drosalys\Bundle\ApiBundle\Action\Info\DeserializeInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\SerializeInfo;
use Drosalys\Bundle\ApiBundle\Action\Type\Type;
use Drosalys\Bundle\ApiBundle\ApiDoc\ContextFixer;
use Drosalys\Bundle\ApiBundle\ApiDoc\Model\ModelRegistry;
use Nelmio\ApiDocBundle\Describer\ModelRegistryAwareInterface;
use Nelmio\ApiDocBundle\Model\ModelRegistry as NelmioRegistry;
use Nelmio\ApiDocBundle\RouteDescriber\RouteDescriberInterface;
use OpenApi\Annotations\AbstractAnnotation;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\OpenApi;
use OpenApi\Annotations\Operation;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\PathItem;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;
use OpenApi\Generator;
use Symfony\Component\Routing\Route;

/**
 * Class AbstractDescriber
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractDescriber implements RouteDescriberInterface, ModelRegistryAwareInterface
{
    private ModelRegistry $modelRegistry;
    private ActionRetriever $actionRetriever;

    private OpenApi $api;
    private Action $action;
    private \ReflectionMethod $reflectionMethod;

    public function setModelRegistry(NelmioRegistry $modelRegistry)
    {
        $this->modelRegistry->setNelmioRegistry($modelRegistry);
    }

    public function setDrosalysModelRegistry(ModelRegistry $modelRegistry): void
    {
        $this->modelRegistry = $modelRegistry;
    }

    public function setActionRetriever(ActionRetriever $actionRetriever): void
    {
        $this->actionRetriever = $actionRetriever;
    }

    final public function getApi(): OpenApi
    {
        return $this->api;
    }

    final public function getReflectionMethod(): \ReflectionMethod
    {
        return $this->reflectionMethod;
    }

    final public function describe(OpenApi $api, Route $route, \ReflectionMethod $reflectionMethod)
    {
        $this->api = $api;
        $this->reflectionMethod = $reflectionMethod;

        if (null !== $action = $this->actionRetriever->retrieveFromReflectionMethod($this->getReflectionMethod(), $route)) {
            $this->action = $action;
            $this->doDescribe($route, $action);
        }
    }

    abstract protected function doDescribe(Route $route, Action $action): void;

    final protected function getAction(): Action
    {
        return $this->action;
    }

    final protected function findPathItem(string $path, bool $create = true): ?PathItem
    {
        foreach ($this->api->paths as $pathItem) {
            if ($pathItem->path === $path) {
                return $pathItem;
            }
        }

        if (!$create) {
            return null;
        }

        return $this->api->paths[] = $this->fixContext(new PathItem([
            'path' => $path,
        ]), $this->api);
    }

    final protected function findOperation(PathItem $pathItem, string $method, bool $create = true): ?Operation
    {
        $classMethod = strtolower($method);

        if (!property_exists($pathItem, $classMethod)) {
            throw new \LogicException(sprintf(
                'Cannot found property "%s" in "%s" class.',
                $classMethod,
                PathItem::class,
            ));
        }

        if (Generator::UNDEFINED === $pathItem->$classMethod) {
            if (!$create) {
                return null;
            }

            if (!class_exists($class = 'OpenApi\\Annotations\\'.ucfirst($method))) {
                throw new \LogicException(sprintf(
                    'Cannot found annotation class for HTTP method "%s".',
                    $method,
                ));
            }

            $pathItem->$classMethod = $this->fixContext(new $class([]), $pathItem);
        }

        return $pathItem->$classMethod;
    }

    final protected function findParameter(Operation $operation, string $name, string $in = 'query', bool $create = true): ?Parameter
    {
        if (Generator::UNDEFINED !== $operation->parameters) {
            foreach ($operation->parameters as $parameter) {
                if (($parameter->name === $name) && ($parameter->in === $in)) {
                    return $parameter;
                }
            }
        } elseif (!$create) {
            return null;
        } else {
            $operation->parameters = [];
        }

        return $operation->parameters[] = $this->fixContext(new Parameter([
            'name' => $name,
            'in' => $in,
        ]), $operation);
    }

    final protected function findRequestBody(Operation $operation, bool $create = true): ?RequestBody
    {
        if (Generator::UNDEFINED !== $operation->requestBody) {
            return $operation->requestBody;
        } elseif (!$create) {
            return null;
        }

        return $operation->requestBody = $this->fixContext(new RequestBody([]), $operation);
    }

    final protected function findResponse(Operation $operation, int $statusCode, bool $create = true): ?Response
    {
        $statusCode = (string) $statusCode;

        if (Generator::UNDEFINED !== $operation->responses) {
            foreach ($operation->responses as $response) {
                if ($response->response === $statusCode) {
                    return $response;
                }
            }
        } elseif (!$create) {
            return null;
        } else {
            $operation->responses = [];
        }

        return $operation->responses[] = $this->fixContext(new Response([
            'response' => $statusCode,
            'description' => '',
        ]), $operation);
    }

    final protected function findMediaType(Response|RequestBody $annotation, string $mineType, bool $create = true): ?MediaType
    {
        if (Generator::UNDEFINED !== $annotation->content) {
            foreach ($annotation->content as $mediaType) {
                if ($mediaType->mediaType === $mineType) {
                    return $mediaType;
                }
            }
        } elseif (!$create) {
            return null;
        } else {
            $annotation->content = [];
        }

        return $annotation->content[] = $this->fixContext(new MediaType([
            'mediaType' => $mineType,
        ]), $annotation);
    }

    final protected function createSchema(SerializeInfo|DeserializeInfo $info, Type $type): Schema
    {
        if (null === $type->getClassName()) {
            $params = [
                'schema' => null,
                'type' => $type->getBuiltin(),
            ];
        } else {
            $params = [
                'schema' => null,
                'ref' => $this->registerModel($info, $type),
            ];
        }

        return new Schema($params);
    }

    final protected function registerModel(SerializeInfo|DeserializeInfo $info, Type $type): string
    {
        return $this->modelRegistry->register($info, $type);
    }

    final protected function getNameFor(SerializeInfo|DeserializeInfo $info, Type $type): string
    {
        return $this->modelRegistry->getNameFor($info, $type);
    }

    final protected function fixContext(AbstractAnnotation $child, AbstractAnnotation $parent): AbstractAnnotation
    {
        return ContextFixer::fixContext($child, $parent);
    }
}
