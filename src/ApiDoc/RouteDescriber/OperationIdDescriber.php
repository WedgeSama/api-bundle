<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber;

use Drosalys\Bundle\ApiBundle\Action\Action;
use OpenApi\Generator;
use Symfony\Component\Routing\Route;

/**
 * Class OperationIdDescriber
 *
 * @author Benjamin Georgeault
 */
class OperationIdDescriber extends AbstractDescriber
{
    protected function doDescribe(Route $route, Action $action): void
    {
        if (
            (null === $pathItem = $this->findPathItem($action->getPath(), false))
            || (null === $operation = $this->findOperation($pathItem, $action->getMethod(), false))
            || Generator::UNDEFINED !== $operation->operationId
        ) {
            return;
        }

        $ref = $this->getReflectionMethod();
        $classRef = $ref->getDeclaringClass();

        $arrayName = explode('\\', preg_replace('/(Controller|Action)$/', '', $classRef->getName()));
        $filteredArrayName = array_filter($arrayName, function (string $part) {
            return !in_array($part, ['Controller', 'Action']);
        });

        if ('__invoke' !== $ref->getName()) {
            $filteredArrayName[] = preg_replace('/(Controller|Action)$/', '', $ref->getName());
        }

        $last = array_pop($filteredArrayName);
        if (strtoupper($last) !== $action->getMethod()) {
            $filteredArrayName[] = $last;
        }

        $ucFirst = array_map('ucfirst', $filteredArrayName);
        $operation->operationId = strtolower($action->getMethod()).implode('', $ucFirst);
    }
}
