<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\ApiDoc\Model;

use Doctrine\Inflector\Inflector;
use Doctrine\Inflector\InflectorFactory;
use Drosalys\Bundle\ApiBundle\Action\Info\DeserializeInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\SerializeInfo;
use Drosalys\Bundle\ApiBundle\Action\Type\Type;
use Nelmio\ApiDocBundle\Model\Model;
use Nelmio\ApiDocBundle\Model\ModelRegistry as NelmioModelRegistry;
use Nelmio\ApiDocBundle\OpenApiPhp\Util;
use Symfony\Component\PropertyInfo\Type as PropertyType;

/**
 * Class ModelRegistry
 *
 * !!! WARNING !!!
 * This class do some dirty actions like accessible some private of a service from NelmioApiDocBundle.
 * This is a temporary workaround to auto register Model without using bundle configuration.
 * !!! WARNING !!!
 *
 *
 * @author Benjamin Georgeault
 * @internal
 */
class ModelRegistry
{
    private Inflector $inflector;
    private NelmioModelRegistry $modelRegistry;

    private \ReflectionProperty $alternativeNamesRef;
    private \ReflectionProperty $namesRef;
    private \ReflectionProperty $registeredModelNamesRef;
    private \ReflectionProperty $apiRef;

    public function __construct(?Inflector $inflector = null)
    {
        $this->inflector = $inflector ?? (InflectorFactory::create())->build();
    }

    public function setNelmioRegistry(NelmioModelRegistry $modelRegistry): static
    {
        $this->modelRegistry = $modelRegistry;

        try {
            $ref = new \ReflectionObject($this->modelRegistry);
            $this->alternativeNamesRef = $ref->getProperty('alternativeNames');
            $this->namesRef = $ref->getProperty('names');
            $this->registeredModelNamesRef = $ref->getProperty('registeredModelNames');
            $this->apiRef = $ref->getProperty('api');
        } catch (\ReflectionException $e) {
            throw new \LogicException(sprintf(
                'There are some changes on class "%s" from NelmioApiDocBundle. The workaround for "%s" has to be updated.',
                NelmioModelRegistry::class,
                self::class,
            ), previous: $e);
        }

        return $this;
    }

    public function register(SerializeInfo|DeserializeInfo $info, Type $type): string
    {
        $model = $this->createModel($info, $type);

        if (!empty($info->getGroups()) && !$this->alternateNameExist($model)) {
            $ref = new \ReflectionClass($type->getClassName());

            $normalizedGroups = array_map([$this->inflector, 'classify'], $info->getGroups());
            $alternativeName = $ref->getShortName().implode('', $normalizedGroups);

            $this->forceAlternativeName($model, $alternativeName);
        }

        return $this->modelRegistry->register($model);
    }

    public function getNameFor(SerializeInfo|DeserializeInfo $info, Type $type): string
    {
        $model = $this->createModel($info, $type);
        $names = $this->getValue($this->namesRef);

        if (array_key_exists($model->getHash(), $names)) {
            return $names[$model->getHash()];
        }

        throw new \LogicException('You must register a model before trying to get its name.');
    }

    private function createModel(SerializeInfo|DeserializeInfo $info, Type $type): Model
    {
        if (null === $class = $type->getClassName()) {
            throw new \InvalidArgumentException('Must have a class for type.');
        }

        return new Model(
            new PropertyType($type->getBuiltin(), false, $class),
            $info->getGroups(),
        );
    }

    // Star dirty unsafe code.

    private function forceAlternativeName(Model $model, string $alternativeName): void
    {
        $alternativeNames = $this->getValue($this->alternativeNamesRef);
        $names = $this->getValue($this->namesRef);
        $registeredModelNames = $this->getValue($this->registeredModelNamesRef);

        $alternativeNames[] = $model;
        $names[$model->getHash()] = $alternativeName;
        $registeredModelNames[$alternativeName] = $model;

        $this->setValue($this->alternativeNamesRef, $alternativeNames);
        $this->setValue($this->namesRef, $names);
        $this->setValue($this->registeredModelNamesRef, $registeredModelNames);

        Util::getSchema($this->getValue($this->apiRef), $alternativeName);
    }

    private function alternateNameExist(Model $model): bool
    {
        return array_key_exists($model->getHash(), $this->getValue($this->namesRef));
    }

    private function getValue(\ReflectionProperty $ref): mixed
    {
        $ref->setAccessible(true);
        $value = $ref->getValue($this->modelRegistry);
        $ref->setAccessible(false);

        return $value;
    }

    private function setValue(\ReflectionProperty $ref, mixed $value): void
    {
        $ref->setAccessible(true);
        $ref->setValue($this->modelRegistry, $value);
        $ref->setAccessible(false);
    }
}
