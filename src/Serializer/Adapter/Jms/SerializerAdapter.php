<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Adapter\Jms;

use Drosalys\Bundle\ApiBundle\Serializer\SerializerInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface as JmsSerializerInterface;

/**
 * Class SerializerAdapter
 *
 * @author Benjamin Georgeault
 */
class SerializerAdapter implements SerializerInterface
{
    public function __construct(
        private JmsSerializerInterface $serializer,
    ) {
    }

    public function serialize($data, string $format, array $groups = [], array $context = []): string
    {
        if (!empty($groups)) {
            $context['groups'] = $groups;
        }

        if (!empty($context)) {
            $jmsContext = SerializationContext::create();
            foreach ($context as $key => $value) {
                $jmsContext->setAttribute($key, $value);
            }
        }

        return $this->serializer->serialize($data, $format, $jmsContext ?? null);
    }

    public function deserialize($data, string $type, string $format, array $groups = [], ?object $objectToPopulate = null, array $context = [])
    {
        if (!empty($groups)) {
            $context['groups'] = $groups;
        }

        if (!empty($context)) {
            $jmsContext = DeserializationContext::create();
            foreach ($context as $key => $value) {
                $jmsContext->setAttribute($key, $value);
            }
        }

        return $this->serializer->deserialize($data, $type, $format);
    }
}
