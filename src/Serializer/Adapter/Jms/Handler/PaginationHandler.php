<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Adapter\Jms\Handler;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigatorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination as SlidingPaginationB;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Pagination\SlidingPagination;

/**
 * Class PaginationHandler
 *
 * @author Benjamin Georgeault
 */
class PaginationHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return array_map(static function (string $type) {
            return [
                'direction' => GraphNavigatorInterface::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => $type,
                'method' => 'serialize',
            ];
        }, [
            SlidingPagination::class,
            SlidingPaginationB::class,
        ]);
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param PaginationInterface $pagination
     * @param array $type
     * @param Context $context
     * @return array|\ArrayObject
     */
    public function serialize(JsonSerializationVisitor $visitor, PaginationInterface $pagination, array $type, Context $context)
    {
        return $visitor->visitArray([
            'page' => $pagination->getCurrentPageNumber(),
            'itemsPerPage' => $pagination->getItemNumberPerPage(),
            'total' => $pagination->getTotalItemCount(),
            'totalPages' => (int) ceil($pagination->getTotalItemCount() / $pagination->getItemNumberPerPage()),
            'items' => $pagination->getItems(),
        ], $type);
    }
}
