<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Adapter\Symfony\Exception;

use Symfony\Component\Serializer\Exception\LogicException;

/**
 * Class EntityException
 *
 * @author Benjamin Georgeault
 */
class EntityException extends LogicException
{
}
