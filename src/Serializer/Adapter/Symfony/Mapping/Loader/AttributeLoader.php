<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Adapter\Symfony\Mapping\Loader;

use Drosalys\Bundle\ApiBundle\Serializer\Attributes\IdGroups;
use Drosalys\Bundle\ApiBundle\Util\ReflectionUtil;
use Symfony\Component\Serializer\Mapping\ClassMetadataInterface;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader as BaseLoader;

/**
 * Class AttributeLoader
 *
 * @author Benjamin Georgeault
 */
class AttributeLoader extends BaseLoader
{
    public function loadClassMetadata(ClassMetadataInterface $classMetadata): bool
    {
        $data = parent::loadClassMetadata($classMetadata);

        if ($data) {
            $ref = $classMetadata->getReflectionClass();
            foreach ($classMetadata->getAttributesMetadata() as $field => $attributeMetadata) {
                try {
                    if (ReflectionUtil::hasAttribute($refProp = $ref->getProperty($field), IdGroups::class)) {
                        /** @var IdGroups $idGroups */
                        $idGroups = ReflectionUtil::getOneAttribute($refProp, IdGroups::class);

                        $context = $attributeMetadata->getNormalizationContextForGroups($idGroups->getGroups());
                        $context['entity_id_groups'] = true;
                        $attributeMetadata->setNormalizationContextForGroups($context, $idGroups->getGroups());
                    }
                } catch (\ReflectionException) {
                }
            }
        }

        return $data;
    }

    /**
     * @param \ReflectionClass|\ReflectionMethod|\ReflectionProperty $reflector
     */
    public function loadAnnotations(object $reflector): iterable
    {
        yield from parent::loadAnnotations($reflector);

        foreach ($reflector->getAttributes() as $attribute) {
            if (IdGroups::class === $attribute->getName()) {
                yield $attribute->newInstance();
            }
        }
    }
}
