<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Adapter\Symfony\Normalizer;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Drosalys\Bundle\ApiBundle\Serializer\Adapter\Symfony\Exception\EntityException;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EntityNormalizer
 *
 * @author Benjamin Georgeault
 */
class EntityNormalizer extends ObjectNormalizer
{
    private EntityManagerInterface $em;

    public function setEm(EntityManagerInterface $em): void
    {
        $this->em = $em;
    }

    public function supportsNormalization($data, string $format = null)
    {
        return
            parent::supportsNormalization($data, $format)
            && $this->em->getMetadataFactory()->hasMetadataFor($data::class)
        ;
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return
            parent::supportsDenormalization($data, $type, $format)
            && $this->em->getMetadataFactory()->hasMetadataFor($type)
        ;
    }

    protected function getAttributeValue(object $object, string $attribute, string $format = null, array $context = []): mixed
    {
        $value = parent::getAttributeValue($object, $attribute, $format, $context);
        if ($context['entity_id_groups'] ?? false) {
            $parentDoctrineMetadata = $this->em->getClassMetadata($object::class);

            if (!$parentDoctrineMetadata->hasAssociation($attribute)) {
                throw new EntityException(sprintf(
                    '"%s::%s" is not an entity association.',
                    $parentDoctrineMetadata->getName(),
                    $attribute,
                ));
            }

            $targetClass = $parentDoctrineMetadata->getAssociationTargetClass($attribute);
            $isTargetSingle = $parentDoctrineMetadata->isSingleValuedAssociation($attribute);
            $targetDoctrineMetadata = $this->em->getClassMetadata($targetClass);

            if ($isTargetSingle) {
                return $this->getIdentifier($targetDoctrineMetadata, $value);
            } else {
                /** @var Collection $value */
                return $value->map(function ($entity) use ($targetDoctrineMetadata) {
                    return $this->getIdentifier($targetDoctrineMetadata, $entity);
                });
            }
        }

        return $value;
    }

    protected function setAttributeValue(object $object, string $attribute, $value, string $format = null, array $context = [])
    {
        parent::setAttributeValue($object, $attribute, $value, $format, $context);
    }

    private function getIdentifier(ClassMetadata $metadata, $object)
    {
        $identifiers = $metadata->getIdentifierValues($object);
        if ($metadata->isIdentifierComposite) {
            return $identifiers;
        }

        return $identifiers[$metadata->getSingleIdentifierFieldName()];
    }
}
