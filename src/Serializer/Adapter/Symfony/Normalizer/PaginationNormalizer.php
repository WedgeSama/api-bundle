<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Adapter\Symfony\Normalizer;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Component\Serializer\NameConverter\AdvancedNameConverterInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class PaginationNormalizer
 *
 * @author Benjamin Georgeault
 */
class PaginationNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    public function __construct(
        private ?NameConverterInterface $nameConverter = null,
    ) {}

    /**
     * @param PaginationInterface $object
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        return [
            $this->normalizeName('page', $format, $context) => $object->getCurrentPageNumber(),
            $this->normalizeName('itemsPerPage', $format, $context) => $object->getItemNumberPerPage(),
            $this->normalizeName('total', $format, $context) => $object->getTotalItemCount(),
            $this->normalizeName('totalPages', $format, $context) => (int) ceil($object->getTotalItemCount() / $object->getItemNumberPerPage()),
            $this->normalizeName('items', $format, $context) => $this->normalizer->normalize($object->getItems(), $format, $context),
        ];
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof PaginationInterface;
    }

    private function normalizeName(string $attribute, string $format = null, array $context = []): string
    {
        if (null === $this->nameConverter) {
            return $attribute;
        }

        if ($this->nameConverter instanceof AdvancedNameConverterInterface) {
            return $this->nameConverter->normalize($attribute, PaginationInterface::class, $format, $context);
        }

        return $this->nameConverter->normalize($attribute);
    }
}
