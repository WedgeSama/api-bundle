<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Adapter\Symfony;

use Drosalys\Bundle\ApiBundle\Serializer\SerializerInterface;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

/**
 * Class SerializerAdapter
 *
 * @author Benjamin Georgeault
 */
class SerializerAdapter implements SerializerInterface
{
    public function __construct(
        private SymfonySerializerInterface $serializer
    ) {
    }

    public function serialize($data, string $format, array $groups = [], array $context = []): string
    {
        if (!empty($groups)) {
            $context['groups'] = $groups;
        }

        return $this->serializer->serialize($data, $format, $context);
    }

    public function deserialize($data, string $type, string $format, array $groups = [], ?object $objectToPopulate = null, array $context = [])
    {
        if (!empty($groups)) {
            $context['groups'] = $groups;
        }

        if (!empty($objectToPopulate)) {
            $context['object_to_populate'] = $objectToPopulate;
        }

        return $this->serializer->deserialize($data, $type, $format, $context);
    }
}
