<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Attributes;

/**
 * Class AbstractSerializable
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractSerializable
{
    private array $groups;

    public function __construct(
        array|string $groups = [],
        private array $context = [],
    ) {
        if (is_string($groups)) {
            $groups = [$groups];
        }

        $this->groups = $groups;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }

    public function getContext(): array
    {
        return $this->context;
    }
}
