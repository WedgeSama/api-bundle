<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Attributes;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class IdGroups
 *
 * @author Benjamin Georgeault
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::IS_REPEATABLE)]
class IdGroups extends Groups
{
}
