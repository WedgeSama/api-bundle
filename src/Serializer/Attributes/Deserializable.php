<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Attributes;

/**
 * Class Deserializable
 *
 * @author Benjamin Georgeault
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class Deserializable extends AbstractSerializable
{
    private bool $isCollection;
    private array|false $validationGroups;

    public function __construct(
        private string $name,
        private ?string $class = null,
        private ?string $collectionClass = null,
        ?bool $isCollection = null,
        array|string $groups = [],
        array|string|false $validationGroups = 'Default',
        array $context = [],
    ) {
        parent::__construct($groups, $context);

        if ($this->collectionClass) {
            $isCollection = true;
        }

        $this->isCollection = $isCollection ?? false;

        $this->validationGroups = is_string($validationGroups) ? (array) $validationGroups : $validationGroups;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function getCollectionClass(): ?string
    {
        return $this->collectionClass;
    }

    public function isCollection(): bool
    {
        return $this->isCollection;
    }

    /**
     * @return string[]|false
     */
    public function getValidationGroups(): array|false
    {
        return $this->validationGroups;
    }
}
