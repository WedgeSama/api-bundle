<?php

/*
 * This file is part of the drosalys-api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer\Attributes;

use Drosalys\Bundle\ApiBundle\Event\Attributes\AbstractAttribute;

/**
 * Class AbstractDeserializeEvent
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractDeserializeEvent extends AbstractAttribute
{
}
