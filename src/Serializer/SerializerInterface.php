<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Serializer;

/**
 * Interface SerializerInterface
 *
 * @author Benjamin Georgeault
 */
interface SerializerInterface
{
    public function serialize($data, string $format, array $groups = [], array $context = []): string;

    public function deserialize($data, string $type, string $format, array $groups = [], ?object $objectToPopulate = null, array $context = []);
}
