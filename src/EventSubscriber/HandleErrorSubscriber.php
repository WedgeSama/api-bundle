<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\EventSubscriber;

use Drosalys\Bundle\ApiBundle\Request\ActionRequestTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class HandleErrorSubscriber
 *
 * @author Benjamin Georgeault
 */
class HandleErrorSubscriber implements EventSubscriberInterface
{
    use ActionRequestTrait;

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['__invoke', -195],
        ];
    }

    public function __invoke(ExceptionEvent $event): void
    {
        if (null === $this->retrieveActionFromRequest($request = $event->getRequest())) {
            return;
        }

        $request->setFormat('json');
    }
}
