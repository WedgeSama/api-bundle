<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle;

use Drosalys\Bundle\ApiBundle\Action\ActionRetriever;
use Drosalys\Bundle\ApiBundle\ApiDoc\Model\ModelRegistry;
use Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber\AbstractDescriber;
use Drosalys\Bundle\ApiBundle\DependencyInjection\Compiler\EntityIdApiDocPass;
use Drosalys\Bundle\ApiBundle\DependencyInjection\Compiler\SymfonySerializerPass;
use Drosalys\Bundle\ApiBundle\Persister\PersisterHandler\PersisterHandlerInterface;
use Drosalys\Bundle\ApiBundle\Response\ResponseHandler\AbstractResponseHandler;
use Drosalys\Bundle\ApiBundle\Response\ResponseHandler\ResponseHandlerInterface;
use Drosalys\Bundle\ApiBundle\Serializer\SerializerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DrosalysApiBundle
 *
 * @author Benjamin Georgeault
 */
class DrosalysApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new SymfonySerializerPass());
        $container->addCompilerPass(new EntityIdApiDocPass());

        $container->registerForAutoconfiguration(AbstractDescriber::class)
            ->addMethodCall('setActionRetriever', [
                new Reference(ActionRetriever::class),
            ])
            ->addMethodCall('setDrosalysModelRegistry', [
                new Reference(ModelRegistry::class),
            ])
        ;

        $container->registerForAutoconfiguration(AbstractResponseHandler::class)
            ->addMethodCall('setSerializer', [
                new Reference(SerializerInterface::class),
            ])
        ;

        $container->registerForAutoconfiguration(ResponseHandlerInterface::class)
            ->addTag('drosalys_api.response.response_handler')
        ;

        $container->registerForAutoconfiguration(PersisterHandlerInterface::class)
            ->addTag('drosalys_api.persister.persister_handler')
        ;
    }
}
