<?php

/*
 * This file is part of the drosalys-api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Response\Attributes;

use Drosalys\Bundle\ApiBundle\Event\Attributes\AbstractAttribute;

/**
 * Class AbstractBuildResponse
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractBuildResponse extends AbstractAttribute
{
}
