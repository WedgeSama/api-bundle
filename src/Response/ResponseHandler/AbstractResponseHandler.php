<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Response\ResponseHandler;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Drosalys\Bundle\ApiBundle\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractResponseHandler
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractResponseHandler implements ResponseHandlerInterface
{
    private SerializerInterface $serializer;

    final public function setSerializer(SerializerInterface $serializer): static
    {
        $this->serializer = $serializer;
        return $this;
    }

    final protected function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }

    final protected function serialize(Action $action, mixed $data, int $statusCode = 200): Response
    {
        $info = $action->getSerializeInfo();
        return new JsonResponse(
            $this->serializer->serialize(
                $data,
                'json',
                $info ? $info->getGroups() : [],
                $info ? $info->getContext() : [],
            ),
            $statusCode,
            [],
            true
        );
    }
}
