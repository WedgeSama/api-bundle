<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Response\ResponseHandler;

use Doctrine\ORM\QueryBuilder;
use Drosalys\Bundle\ApiBundle\Action\Action;
use Drosalys\Bundle\ApiBundle\Filter\ApiFilterManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PaginationResponseHandler
 *
 * @author Benjamin Georgeault
 */
class PaginationResponseHandler extends AbstractResponseHandler
{
    public function __construct(
        private PaginatorInterface $paginator,
        private ApiFilterManager $filterUpdater,
    ) {
    }

    public function support(Action $action, Request $request, mixed $data): bool
    {
        return null !== $action->getPaginationInfo();
    }

    public function buildResponse(Action $action, Request $request, mixed $data): Response
    {
        if (null !== $filterInfo = $action->getFilterInfo()) {
            if (!$data instanceof QueryBuilder) {
                throw new \LogicException(sprintf(
                    'Current controller must return an instance of "%s" to use Filter.',
                    QueryBuilder::class,
                ));
            }

            $this->filterUpdater->applyFilter($data, $request, $filterInfo);
        }

        $paginationInfo = $action->getPaginationInfo();

        $pagination = $this->paginator->paginate(
            $data,
            max(
                1,
                $request->get($paginationInfo->getPageName(), 1),
            ),
            max(
                1,
                min(
                    $request->get($paginationInfo->getLimitName(), $paginationInfo->getLimit()),
                    $paginationInfo->getMaxLimit(),
                ),
            ),
            $paginationInfo->getOptions(),
        );

        return $this->serialize($action, $pagination);
    }
}
