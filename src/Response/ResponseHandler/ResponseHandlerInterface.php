<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Response\ResponseHandler;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface ResponseHandlerInterface
 *
 * @author Benjamin Georgeault
 */
interface ResponseHandlerInterface
{
    public function support(Action $action, Request $request, mixed $data): bool;

    public function buildResponse(Action $action, Request $request, mixed $data): Response;
}
