<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Response\ResponseHandler;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EmptyResponseHandler
 *
 * @author Benjamin Georgeault
 */
class EmptyResponseHandler implements ResponseHandlerInterface
{
    public function support(Action $action, Request $request, mixed $data): bool
    {
        return null === $data;
    }

    public function buildResponse(Action $action, Request $request, mixed $data): Response
    {
        // If null get, it must be 404 not found.
        $statusCode = 404;
        if (null !== $info = $action->getSerializeInfo()) {
            if (null !== $type = $info->getType()) {
                if ($type->isVoid()) {
                    // Except if the return type of controller is void.
                    $statusCode = 204;
                }
            }
        }

        return new JsonResponse(null, $statusCode);
    }
}
