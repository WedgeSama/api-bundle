<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Response\ResponseHandler;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Drosalys\Bundle\ApiBundle\EventSubscriber\DeserializeActionControllerSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ValidationErrorResponseHandler
 *
 * @author Benjamin Georgeault
 */
class ValidationErrorResponseHandler extends AbstractResponseHandler
{
    public function support(Action $action, Request $request, mixed $data): bool
    {
        return $request->attributes->has(DeserializeActionControllerSubscriber::REQUEST_ERROR_KEY);
    }

    public function buildResponse(Action $action, Request $request, mixed $data): Response
    {
        return $this->serialize(
            $action,
            $request->attributes->get(DeserializeActionControllerSubscriber::REQUEST_ERROR_KEY),
            400,
        );
    }
}
