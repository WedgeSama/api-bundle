<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\DependencyInjection;

use Drosalys\Bundle\ApiBundle\ApiDoc\RouteDescriber\TagDescriber;
use Drosalys\Bundle\ApiBundle\Serializer\Adapter\Symfony\Normalizer\PaginationNormalizer;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DrosalysApiExtension
 *
 * @author Benjamin Georgeault
 */
class DrosalysApiExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        // Load serializer services.
        $loader->load(('jms' === $config['serializer']) ? 'jms_serializer.yaml' : 'symfony_serializer.yaml');

        $container->getDefinition(TagDescriber::class)
            ->setArgument(0, $config['tag_namespaces'])
        ;

        if ('symfony' === $config['serializer']) {
            $container->getDefinition(PaginationNormalizer::class)
                ->setArgument(0, new Reference('serializer.name_converter.camel_case_to_snake_case'))
            ;
        }
    }
}
