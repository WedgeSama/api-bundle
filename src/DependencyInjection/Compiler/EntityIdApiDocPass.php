<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\DependencyInjection\Compiler;

use Doctrine\ORM\EntityManagerInterface;
use Drosalys\Bundle\ApiBundle\ApiDoc\PropertyDescriber\EntityIdPropertyDescriber;
use Nelmio\ApiDocBundle\PropertyDescriber\ObjectPropertyDescriber;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class EntityIdApiDocPass
 *
 * @author Benjamin Georgeault
 */
class EntityIdApiDocPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('nelmio_api_doc.object_model.property_describers.object')) {
            return;
        }

        $objectPropertyDescriber = $container->getDefinition('nelmio_api_doc.object_model.property_describers.object');
        if (ObjectPropertyDescriber::class === $objectPropertyDescriber->getClass()) {
            $objectPropertyDescriber
                ->setClass(EntityIdPropertyDescriber::class)
                ->addMethodCall('setEm', [
                    new Reference(EntityManagerInterface::class),
                ])
            ;
        }
    }
}
