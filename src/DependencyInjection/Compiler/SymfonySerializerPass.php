<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\DependencyInjection\Compiler;

use Drosalys\Bundle\ApiBundle\Serializer\Adapter\Symfony\Mapping\Loader\AttributeLoader;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader as SymfonyAnnotationLoader;

/**
 * Class SymfonySerializerPass
 *
 * @author Benjamin Georgeault
 */
class SymfonySerializerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasParameter('drosalys_api.serializer.symfony') || !$container->getParameter('drosalys_api.serializer.symfony')) {
            return;
        }

        if ($container->hasDefinition('serializer.mapping.chain_loader')) {
            $chainLoader = $container->getDefinition('serializer.mapping.chain_loader');
            /** @var Definition[] $serializerLoaders */
            $serializerLoaders = $chainLoader->getArgument(0);

            foreach ($serializerLoaders as $serializerLoader) {
                if (SymfonyAnnotationLoader::class === $serializerLoader->getClass()) {
                    $serializerLoader
                        ->setClass(AttributeLoader::class)
                    ;
                }
            }
        }
    }
}
