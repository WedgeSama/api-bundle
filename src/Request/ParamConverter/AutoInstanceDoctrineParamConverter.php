<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Request\ParamConverter;

use Drosalys\Bundle\ApiBundle\Request\ActionRequestTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AutoInstanceDoctrineParamConverter
 *
 * @author Benjamin Georgeault
 */
class AutoInstanceDoctrineParamConverter implements ParamConverterInterface
{
    use ActionRequestTrait;

    public function __construct(
        private DoctrineParamConverter $converter,
    ) {
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        if (
            (null !== $action = $this->retrieveActionFromRequest($request))
            && $action->isMethod('POST')
            && (null !== $deserializeInfo = $action->getDeserializeInfo())
            && $configuration->getName() === $deserializeInfo->getName()
        ) {
            $class = $deserializeInfo->getType()->getClassName();
            $request->attributes->set($configuration->getName(), new $class());

            return true;
        }

        return $this->converter->apply($request, $configuration);
    }

    public function supports(ParamConverter $configuration)
    {
        return $this->converter->supports($configuration);
    }
}
