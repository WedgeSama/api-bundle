<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Request;

use Drosalys\Bundle\ApiBundle\Action\Action;
use Drosalys\Bundle\ApiBundle\EventSubscriber\ActionControllerSubscriber;
use Symfony\Component\HttpFoundation\Request;

/**
 * Trait ActionRequestTrait
 *
 * @author Benjamin Georgeault
 */
trait ActionRequestTrait
{
    private function retrieveActionFromRequest(Request $request): ?Action
    {
        if (!$request->attributes->has(ActionControllerSubscriber::REQUEST_KEY)) {
            return null;
        }

        $action = $request->attributes->get(ActionControllerSubscriber::REQUEST_KEY);

        if (!$action instanceof Action) {
            throw new \LogicException('Must be action instance.');
        }

        return $action;
    }
}
