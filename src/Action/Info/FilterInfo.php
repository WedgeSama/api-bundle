<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action\Info;

/**
 * Class FilterInfo
 *
 * @author Benjamin Georgeault
 */
final class FilterInfo
{
    public function __construct(
        private string $filterType,
        private array $options,
        private string $key,
    ) {
    }

    public function getFilterType(): string
    {
        return $this->filterType;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getKey(): string
    {
        return $this->key;
    }
}
