<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action\Info;

/**
 * Class PaginationInfo
 *
 * @author Benjamin Georgeault
 */
final class PaginationInfo
{
    public function __construct(
        private int $limit,
        private int $maxLimit,
        private string $pageName,
        private string $limitName,
        private array $options = [],
    ) {
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getMaxLimit(): int
    {
        return $this->maxLimit;
    }

    public function getPageName(): string
    {
        return $this->pageName;
    }

    public function getLimitName(): string
    {
        return $this->limitName;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
