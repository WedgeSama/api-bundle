<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action\Info;

/**
 * Class EventInfo
 *
 * @author Benjamin Georgeault
 */
final class EventInfo
{
    private ?object $controller = null;

    public function __construct(
        private ?string $preMethod = null,
        private ?string $postMethod = null,
        private ?string $replaceMethod = null,
    ) {
    }

    public function hasPre(): bool
    {
        return null !== $this->preMethod;
    }

    public function getPre(): ?callable
    {
        if (null === $this->preMethod) {
            return null;
        }

        return [$this->getController(), $this->preMethod];
    }

    public function getPreMethod(): ?string
    {
        return $this->preMethod;
    }

    public function hasPost(): bool
    {
        return null !== $this->postMethod;
    }

    public function getPost(): ?callable
    {
        if (null === $this->postMethod) {
            return null;
        }

        return [$this->getController(), $this->postMethod];
    }

    public function getPostMethod(): ?string
    {
        return $this->postMethod;
    }

    public function hasReplace(): bool
    {
        return null !== $this->replaceMethod;
    }

    public function getReplace(): ?callable
    {
        if (null === $this->replaceMethod) {
            return null;
        }

        return [$this->getController(), $this->replaceMethod];
    }

    public function getReplaceMethod(): ?string
    {
        return $this->replaceMethod;
    }

    public function setController(object $controller): self
    {
        if (null !== $this->controller) {
            throw new \LogicException('Controller already set.');
        }

        $this->controller = $controller;

        return $this;
    }

    private function getController(): object
    {
        if (null === $this->controller) {
            throw new \LogicException('Controller not set.');
        }

        return $this->controller;
    }
}
