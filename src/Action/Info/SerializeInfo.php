<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action\Info;

use Drosalys\Bundle\ApiBundle\Action\Type\Type;

/**
 * Class SerializeInfo
 *
 * @author Benjamin Georgeault
 */
final class SerializeInfo
{
    public function __construct(
        private array $groups = [],
        private ?Type $type = null,
        private array $context = [],
    ) {
    }

    /**
     * @return string[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function getContext(): array
    {
        return $this->context;
    }
}
