<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action\Info;

use Drosalys\Bundle\ApiBundle\Action\Type\Type;

/**
 * Class DeserializeInfo
 *
 * @author Benjamin Georgeault
 */
final class DeserializeInfo
{
    public function __construct(
        private string $name,
        private array $groups = [],
        private ?Type $type = null,
        private array|false $validationGroups = ['Default'],
        private array $context = [],
        private ?EventInfo $eventInfo = null,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    /**
     * @return string[]|false
     */
    public function getValidationGroups(): array|false
    {
        return $this->validationGroups;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function getEventInfo(): ?EventInfo
    {
        return $this->eventInfo;
    }
}
