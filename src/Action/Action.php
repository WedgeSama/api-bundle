<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action;

use Drosalys\Bundle\ApiBundle\Action\Info\DeserializeInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\EventInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\FilterInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\PaginationInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\SerializeInfo;

/**
 * Class Action
 *
 * @author Benjamin Georgeault
 */
final class Action
{
    private string $method;

    public function __construct(
        string $method = 'GET',
        private string $path = '/',
        private SerializeInfo|null $serializeInfo = null,
        private DeserializeInfo|null $deserializeInfo = null,
        private ?PaginationInfo $paginationInfo = null,
        private ?FilterInfo $filterInfo = null,
        private bool $hasSecurity = false,
        private ?EventInfo $persistInfo = null,
        private ?EventInfo $buildResponseInfo = null,
    ) {
        $this->method = strtoupper($method);
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function isMethod(string $method): bool
    {
        return strtoupper($method) === $this->method;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getSerializeInfo(): SerializeInfo|null
    {
        return $this->serializeInfo;
    }

    public function getDeserializeInfo(): DeserializeInfo|null
    {
        return $this->deserializeInfo;
    }

    public function getPaginationInfo(): ?PaginationInfo
    {
        return $this->paginationInfo;
    }

    public function getFilterInfo(): ?FilterInfo
    {
        return $this->filterInfo;
    }

    public function hasSecurity(): bool
    {
        return $this->hasSecurity;
    }

    public function getPersistInfo(): ?EventInfo
    {
        return $this->persistInfo;
    }

    public function getBuildResponseInfo(): ?EventInfo
    {
        return $this->buildResponseInfo;
    }

    public function setController(object $controller): static
    {
        $this->getPersistInfo()?->setController($controller);
        $this->getBuildResponseInfo()?->setController($controller);
        $this->getDeserializeInfo()?->getEventInfo()?->setController($controller);

        return $this;
    }

    public function needController(): bool
    {
        return (null !== $this->getPersistInfo())
            || (null !== $this->getBuildResponseInfo())
            || (null !== $this->getDeserializeInfo() && null !== $this->getDeserializeInfo()->getEventInfo())
        ;
    }
}
