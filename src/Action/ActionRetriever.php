<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action;

use Drosalys\Bundle\ApiBundle\Action\Info\DeserializeInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\EventInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\FilterInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\PaginationInfo;
use Drosalys\Bundle\ApiBundle\Action\Info\SerializeInfo;
use Drosalys\Bundle\ApiBundle\Action\Type\ReturnLoaderInterface;
use Drosalys\Bundle\ApiBundle\Event\Attributes\AbstractAttribute;
use Drosalys\Bundle\ApiBundle\Filter\Attributes\Filterable;
use Drosalys\Bundle\ApiBundle\Pagination\Attributes\Paginable;
use Drosalys\Bundle\ApiBundle\Persister\Attributes\AbstractPersist;
use Drosalys\Bundle\ApiBundle\Persister\Attributes\PostPersist;
use Drosalys\Bundle\ApiBundle\Persister\Attributes\PrePersist;
use Drosalys\Bundle\ApiBundle\Persister\Attributes\ReplacePersist;
use Drosalys\Bundle\ApiBundle\Response\Attributes\AbstractBuildResponse;
use Drosalys\Bundle\ApiBundle\Response\Attributes\PostBuildResponse;
use Drosalys\Bundle\ApiBundle\Response\Attributes\PreBuildResponse;
use Drosalys\Bundle\ApiBundle\Response\Attributes\ReplaceBuildResponse;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\AbstractRoute;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\AbstractDeserializeEvent;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Deserializable;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\PostDeserialize;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\PreDeserialize;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;
use Drosalys\Bundle\ApiBundle\Util\ReflectionUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Route;

/**
 * Class ActionRetriever
 *
 * @author Benjamin Georgeault
 */
final class ActionRetriever
{
    /**
     * ActionRetriever constructor.
     * @param iterable|ReturnLoaderInterface[] $returnLoaders
     */
    public function __construct(
        private iterable $returnLoaders,
    ) {
    }

    public function hasActionController(callable $controller): bool
    {
        /** @var \ReflectionClass $refClass */
        /** @var \ReflectionMethod $refMethod */
        list($refClass, $refMethod) = $this->getReflections($controller);

        return $this->hasActionReflectionMethod($refMethod);
    }

    public function hasActionReflectionMethod(\ReflectionMethod $refMethod): bool
    {
        return ReflectionUtil::hasAttribute($refMethod, AbstractRoute::class);
    }

    public function retrieveFromController(callable $controller, Route $route): ?Action
    {
        /** @var \ReflectionClass $refClass */
        /** @var \ReflectionMethod $refMethod */
        list($refClass, $refMethod) = $this->getReflections($controller);

        return $this->retrieveFromReflectionMethod($refMethod, $route);
    }

    public function retrieveFromReflectionMethod(\ReflectionMethod $refMethod, Route $sfRoute): ?Action
    {
        /** @var AbstractRoute|null $route */
        if (null === $route = ReflectionUtil::getOneAttribute($refMethod, AbstractRoute::class)) {
            return null;
        }

        // Try to guess return type.
        $returnType = new Type\Type();

        if (ReflectionUtil::hasAttribute($refMethod, Paginable::class)) {
            /** @var Paginable $paginable */
            $paginable = ReflectionUtil::getOneAttribute($refMethod, Paginable::class);

            $paginationInfo = new PaginationInfo(
                limit: $paginable->getLimit() ?? 10,
                maxLimit: $paginable->getMaxLimit() ?? 200,
                pageName: $paginable->getPageName() ?? 'page',
                limitName: $paginable->getLimitName() ?? 'limit',
                options: $paginable->getOptions() ?? [],
            );

            if (!class_exists($innerType = $paginable->getInnerType())) {
                throw new \LogicException(sprintf(
                    'Paginable attribute handle only one class for now. Error on method %s::%s.',
                    $refMethod->getDeclaringClass(),
                    $refMethod->getName(),
                ));
            }

            $returnType
                ->setClassName($innerType)
                ->setNullable(false)
            ;
        } else {
            foreach ($this->returnLoaders as $returnLoader) {
                $returnLoader->load($refMethod, $returnType);
            }
        }

        if (ReflectionUtil::hasAttribute($refMethod, Filterable::class)) {
            if (!isset($paginationInfo)) {
                throw new \LogicException(sprintf(
                    '"%s" attribute require a "%s" attribute too. Error on method %s::%s.',
                    Filterable::class,
                    Paginable::class,
                    $refMethod->getDeclaringClass(),
                    $refMethod->getName(),
                ));
            }

            /** @var Filterable $filterable */
            $filterable = ReflectionUtil::getOneAttribute($refMethod, Filterable::class);

            $filterInfo = new FilterInfo(
                filterType: $filterable->getFilterType(),
                options: $filterable->getOptions(),
                key: $filterable->getKey() ?? 'f',
            );
        }

        /** @var Serializable|null $serializable */
        $serializable = ReflectionUtil::getOneAttribute($refMethod, Serializable::class);
        $serializeInfo = new SerializeInfo(
            groups: $serializable ? $serializable->getGroups() : [],
            type: $returnType,
            context: $serializable ? $serializable->getContext() : [],
        );

        /** @var Deserializable|null $deserializable */
        if (null !== $deserializable = ReflectionUtil::getOneAttribute($refMethod, Deserializable::class)) {
            $dataType = (new Type\Type())
                ->setNullable(false)
            ;

            if (null === $class = $deserializable->getClass()) {
                try {
                    $class = $this->getParameterClass($refMethod, $deserializable->getName());
                } catch (\Exception $e) {
                    throw new \LogicException(sprintf(
                        'Deserializable class must exist. Error on method %s::%s.',
                        $refMethod->getDeclaringClass(),
                        $refMethod->getName(),
                    ), previous: $e);
                }
            }

            $dataType->setClassName($class);

            // Wrap in collection (array or iterable object).
            if ($deserializable->isCollection()) {
                $collectionType = (new Type\Type())
                    ->setNullable(false)
                ;

                if (null === $collectionClass = $deserializable->getCollectionClass()) {
                    $collectionType->setBuiltin(Type\Type::BUILTIN_TYPE_ARRAY);
                } else {
                    $collectionType->setClassName($collectionClass);
                }

                $collectionType->setWrap($dataType);
                $dataType = $collectionType;
            }

            $deserializeInfo = $this->getEventInfo(
                $refMethod,
                AbstractDeserializeEvent::class,
                PreDeserialize::class,
                PostDeserialize::class,
            );

            $deserializeInfo = new DeserializeInfo(
                name: $deserializable->getName(),
                groups: $deserializable->getGroups() ?? [],
                type: $dataType,
                validationGroups: $deserializable->getValidationGroups(),
                context: $deserializable->getContext() ?? [],
                eventInfo: $deserializeInfo,
            );
        }

        if (
            ReflectionUtil::hasAttribute($refMethod, IsGranted::class)
            || ReflectionUtil::hasAttribute($refMethod, Security::class)
            || ReflectionUtil::hasAttribute($refMethod->getDeclaringClass(), IsGranted::class)
            || ReflectionUtil::hasAttribute($refMethod->getDeclaringClass(), Security::class)
        ) {
            $hasSecurity = true;
        }

        $persistInfo = $this->getEventInfo(
            $refMethod,
            AbstractPersist::class,
            PrePersist::class,
            PostPersist::class,
            ReplacePersist::class,
        );

        $buildResponseInfo = $this->getEventInfo(
            $refMethod,
            AbstractBuildResponse::class,
            PreBuildResponse::class,
            PostBuildResponse::class,
            ReplaceBuildResponse::class,
        );

        if ((null !== $buildResponseInfo) && $buildResponseInfo->hasReplace()) {
            $refBuildResponse = $refMethod->getDeclaringClass()->getMethod($buildResponseInfo->getReplaceMethod());

            if (
                (null === $returnType = $refBuildResponse->getReturnType())
                || (!$returnType instanceof \ReflectionNamedType)
                || !is_a($returnType->getName(), Response::class)
            ) {
                throw new \LogicException(sprintf(
                    'Return type must be set to "%s" on method %s::%s.',
                    Response::class,
                    $refBuildResponse->getDeclaringClass(),
                    $refBuildResponse->getName(),
                ));
            }
        }

        return new Action(
            method: $route->getMethod(),
            path: $sfRoute->getPath(),
            serializeInfo: $serializeInfo ?? null,
            deserializeInfo: $deserializeInfo ?? null,
            paginationInfo: $paginationInfo ?? null,
            filterInfo: $filterInfo ?? null,
            hasSecurity: $hasSecurity ?? false,
            persistInfo: $persistInfo,
            buildResponseInfo: $buildResponseInfo,
        );
    }

    /**
     * @param callable $controller
     * @return \ReflectionClass[]|\ReflectionMethod[]
     */
    private function getReflections(callable $controller): array
    {
        if (is_object($controller) && method_exists($controller, '__invoke')) {
            $ctrl = $controller;
            $method = '__invoke';
        } elseif (is_array($controller)) {
            list($ctrl, $method) = $controller;
        } else {
            throw new \LogicException('Cannot found controller.');
        }

        $classRef = new \ReflectionClass(get_class($ctrl));
        $methodRef = $classRef->getMethod($method);

        return [$classRef, $methodRef];
    }

    private function getParameterClass(\ReflectionMethod $ref, string $argumentName): string
    {
        foreach ($ref->getParameters() as $paramRef) {
            if (
                (null === $type = $paramRef->getType())
                || !$type instanceof \ReflectionNamedType
                || $argumentName !== $paramRef->getName()
            ) {
                continue;
            }

            if (class_exists($type->getName())) {
                return $type->getName();
            }

            break;
        }

        throw new \LogicException(sprintf(
            'Cannot guess class type of parameter "%s" for method "%s::%s".',
            $argumentName,
            $ref->getDeclaringClass()->getName(),
            $ref->getName(),
        ));
    }

    private function getEventInfo(\ReflectionMethod $refMethod, string $abstractClass, string $preClass, string $postClass, ?string $replaceClass = null): ?EventInfo
    {
        if (ReflectionUtil::hasAttribute($refMethod, $abstractClass)) {
            /** @var AbstractAttribute|null $pre */
            $pre = ReflectionUtil::getOneAttribute($refMethod, $preClass);
            /** @var AbstractAttribute|null $post */
            $post = ReflectionUtil::getOneAttribute($refMethod, $postClass);

            if (null !== $replaceClass) {
                /** @var AbstractAttribute|null $replace */
                $replace = ReflectionUtil::getOneAttribute($refMethod, $replaceClass);

                if (null !== $replace) {
                    if (null !== $pre) {
                        throw new \LogicException(sprintf(
                            'Cannot have both REPLACE and PRE event for method "%s::%s".',
                            $refMethod->getDeclaringClass()->getName(),
                            $refMethod->getName(),
                        ));
                    }

                    if (null !== $post) {
                        throw new \LogicException(sprintf(
                            'Cannot have both REPLACE and POST event for method "%s::%s".',
                            $refMethod->getDeclaringClass()->getName(),
                            $refMethod->getName(),
                        ));
                    }

                    $replaceMethod = $replace->getMethod();
                }
            }

            return new EventInfo(
                preMethod: $pre ? $pre->getMethod() : null,
                postMethod: $post ? $post->getMethod() : null,
                replaceMethod: $replaceMethod ?? null,
            );
        }

        return null;
    }
}
