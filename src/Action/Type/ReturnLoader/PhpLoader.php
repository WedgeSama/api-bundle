<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action\Type\ReturnLoader;

use Drosalys\Bundle\ApiBundle\Action\Type\ReturnLoaderInterface;
use Drosalys\Bundle\ApiBundle\Action\Type\Type;

/**
 * Class PhpLoader
 *
 * @author Benjamin Georgeault
 */
class PhpLoader implements ReturnLoaderInterface
{
    public function load(\ReflectionMethod $refMethod, Type $type): void
    {
        if (null === $returnType = $refMethod->getReturnType()) {
            return;
        }

        if ($returnType->allowsNull() && null === $type->isNullable()) {
            $type->setNullable(true);
        }

        if ($returnType instanceof \ReflectionNamedType) {
            $this->populateTypeFromReflectionNamedType($type, $returnType);
        }

        if ($returnType instanceof \ReflectionUnionType) {
            foreach ($returnType->getTypes() as $uReturnType) {
                $type->addType($this->populateTypeFromReflectionNamedType(new Type(), $uReturnType));
            }
        }
    }

    private function populateTypeFromReflectionNamedType(Type $type, \ReflectionNamedType $ref): Type
    {
        if ($ref->isBuiltin()) {
            $type->setBuiltin($ref->getName());
        } else {
            $type->setClassName($ref->getName());
        }

        return $type;
    }
}
