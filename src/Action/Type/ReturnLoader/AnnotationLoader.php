<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action\Type\ReturnLoader;

use Drosalys\Bundle\ApiBundle\Action\Type\ReturnLoaderInterface;
use Drosalys\Bundle\ApiBundle\Action\Type\Type;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use phpDocumentor\Reflection\DocBlockFactory;
use phpDocumentor\Reflection\Types\AbstractList;
use phpDocumentor\Reflection\Types\Compound;
use phpDocumentor\Reflection\Types\ContextFactory;
use phpDocumentor\Reflection\Type as PhpDocType;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\Object_;

/**
 * Class AnnotationLoader
 *
 * @author Benjamin Georgeault
 */
class AnnotationLoader implements ReturnLoaderInterface
{
    private DocBlockFactory $docBlockFactory;
    private ContextFactory $contextFactory;

    public function __construct()
    {
        $this->docBlockFactory = DocBlockFactory::createInstance();
        $this->contextFactory = new ContextFactory();
    }

    public function load(\ReflectionMethod $refMethod, Type $type): void
    {
        if (false === $doc = $refMethod->getDocComment()) {
            return;
        }

        $refClass = $refMethod->getDeclaringClass();
        $namespace = $refClass->getNamespaceName();

        $context = $this->contextFactory->createForNamespace(
            $namespace,
            file_get_contents($refClass->getFileName())
        );

        $docBlock = $this->docBlockFactory->create($doc, $context);

        /** @var Return_[] $returns */
        $returns = $docBlock->getTagsByName('return');

        foreach ($returns as $return) {
            if (null !== $docReturnType = $return->getType()) {
                $this->parseType($docReturnType, $type);
            }
        }
    }

    private function parseType(PhpDocType $phpDocType, Type $type): Type
    {
        if ($phpDocType instanceof Compound) {
            foreach ($phpDocType as $subPhpDocType) {
                $type->addType($this->parseType($subPhpDocType, new Type()));
            }
        } elseif ($phpDocType instanceof AbstractList) {
            $type->addType($this->parseType($phpDocType->getValueType(), new Type()));
        } elseif ($phpDocType instanceof Object_) {
            if (null === $class = $phpDocType->getFqsen()) {
                $type->setBuiltin(Type::BUILTIN_TYPE_OBJECT);
            } else {
                $type->setClassName($class);
            }
        } elseif ($phpDocType instanceof Null_) {
            $type->setNullable(true);
        } elseif (in_array($key = $phpDocType->__toString(), Type::$builtinTypes)) {
            $type->setBuiltin($key);
        }

        return $type;
    }
}
