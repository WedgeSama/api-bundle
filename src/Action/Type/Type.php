<?php

/*
 * This file is part of the drosalys/api-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\Bundle\ApiBundle\Action\Type;

/**
 * Class Type
 *
 * @author Benjamin Georgeault
 */
final class Type
{
    public const BUILTIN_TYPE_INT = 'int';
    public const BUILTIN_TYPE_FLOAT = 'float';
    public const BUILTIN_TYPE_STRING = 'string';
    public const BUILTIN_TYPE_BOOL = 'bool';
    public const BUILTIN_TYPE_TRUE = 'true';
    public const BUILTIN_TYPE_FALSE = 'false';
    public const BUILTIN_TYPE_OBJECT = 'object';
    public const BUILTIN_TYPE_ARRAY = 'array';
    public const BUILTIN_TYPE_NULL = 'null';
    public const BUILTIN_TYPE_VOID = 'void';
    public const BUILTIN_TYPE_ITERABLE = 'iterable';

    public static array $builtinTypes = [
        self::BUILTIN_TYPE_INT,
        self::BUILTIN_TYPE_FLOAT,
        self::BUILTIN_TYPE_STRING,
        self::BUILTIN_TYPE_BOOL,
        self::BUILTIN_TYPE_TRUE,
        self::BUILTIN_TYPE_FALSE,
        self::BUILTIN_TYPE_OBJECT,
        self::BUILTIN_TYPE_ARRAY,
        self::BUILTIN_TYPE_NULL,
        self::BUILTIN_TYPE_VOID,
        self::BUILTIN_TYPE_ITERABLE,
    ];

    private static array $iterableBuiltinTypes = [
        self::BUILTIN_TYPE_ITERABLE,
        self::BUILTIN_TYPE_ARRAY,
    ];

    private ?bool $nullable = null;
    private ?string $builtin = null;
    private ?string $className = null;
    /** @var self[]|null */
    private ?array $types = null;

    private ?self $wrap = null;

    public function is(): ?bool
    {
        if (null !== $this->builtin) {

        }

        return null;
    }

    public function isVoid(): bool
    {
        return self::BUILTIN_TYPE_VOID === $this->builtin;
    }

    public function isNullable(): ?bool
    {
        return $this->nullable;
    }

    public function setNullable(bool $nullable): self
    {
        $this->nullable = $nullable;
        return $this;
    }

    public function isIterable(): bool
    {
        if (null === $this->builtin) {
            return false;
        }

        return in_array($this->builtin, self::$iterableBuiltinTypes);
    }

    public function isItemable(): ?bool
    {
        return !$this->isIterable();
    }

    public function getBuiltin(): ?string
    {
        return $this->builtin;
    }

    public function setBuiltin(string $builtin): self
    {
        if (!in_array($builtin, self::$builtinTypes)) {
            throw new \InvalidArgumentException(sprintf(
                'Builtin type "%s" not allowed. Use one of: %s.',
                $builtin,
                implode(', ', self::$builtinTypes),
            ));
        }

        $this->builtin = $builtin;

        if (self::BUILTIN_TYPE_NULL === $builtin) {
            $this->nullable = true;
        }

        return $this;
    }

    public function getClassName(): ?string
    {
        return $this->className;
    }

    public function setClassName(?string $className): self
    {
        if (!class_exists($className)) {
            throw new \InvalidArgumentException(sprintf(
                'Given "%s" is not an existing class.',
                $className,
            ));
        }

        $this->className = $className;
        $this->builtin = 'object';
        return $this;
    }

    /**
     * @return self[]|null
     */
    public function getTypes(): ?array
    {
        return array_values($this->types);
    }

    public function addType(self $type): self
    {
        // If nullable is added, just set nullable to true.
        if (true === $type->isNullable()) {
            $this->nullable = true;
            return $this;
        }

        // If same Type added, just merge.
        if (
            (null !== $type->className && $type->className === $this->className)
            || (null !== $type->builtin && $type->builtin === $this->builtin)
        ) {
            $this->merge($type);
            return $this;
        }

        // Init array.
        if (null === $this->types) {
            $this->types = [];

            // If already builtin type set, clone it and add as union.
            if (null !== $this->builtin) {
                $clone = clone $this;
                $clone->nullable = null;
                $clone->types = null;

                $this->addType($clone);
                $this->builtin = null;
                $this->className = null;
            }
        }

        if (null === $key = $type->className ?? $type->builtin) {
            throw new \InvalidArgumentException('Cannot addType without at least a class nor a builtin value set.');
        }

        if (array_key_exists($key, $this->types)) {
            $this->types[$key]->merge($type);
        } else {
            $this->types[$key] = $type;
        }

        return $this;
    }

    public function isBuiltin(): bool
    {
        return
            (null === $this->className)
            && (null === $this->types)
            && (in_array($this->builtin, self::$builtinTypes))
        ;
    }

    public function isUnion(): bool
    {
        return !empty($this->types);
    }

    public function isClass(): bool
    {
        return null !== $this->className;
    }

    public function merge(self $type): self
    {
        if (
            (null === $this->nullable)
            && (null !== $type->nullable)
        ) {
            $this->nullable = $type->nullable;
        }

        if (
            (null === $this->builtin)
            && (null !== $type->builtin)
        ) {
            $this->builtin = $type->builtin;
        }

        if (
            (null === $this->className)
            && (null !== $type->className)
        ) {
            $this->setClassName($type->className);
        }

        if (null !== $type->types) {
            foreach ($type->types as $uType) {
                $this->addType($uType);
            }
        }

        return $this;
    }

    public function setWrap(self $wrap): self
    {
        $ok = false;
        if ($this->isIterable()) {
            $ok = true;
        } else {
            if (null !== $class = $this->getClassName()) {
                $ref = new \ReflectionClass($class);
                if ($ref->implementsInterface(\Traversable::class)) {
                    $ok = true;
                }
            }
        }

        if (!$ok) {
            throw new \InvalidArgumentException('Given Type has to be an iterable, array or an object implementing Traversable interface to be a wrapper.');
        }

        $this->wrap = $wrap;

        return $this;
    }

    public function hasWrap(): bool
    {
        return null !== $this->wrap;
    }

    public function getWrap(): ?self
    {
        return $this->wrap;
    }
}
